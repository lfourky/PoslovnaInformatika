USE [master]
GO
/****** Object:  Database [poslovnaInformatika]    Script Date: 4/25/2016 5:35:00 PM ******/
CREATE DATABASE [poslovnaInformatika]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'poslovnaInformatika', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\poslovnaInformatika.mdf' , SIZE = 3136KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'poslovnaInformatika_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\poslovnaInformatika_log.ldf' , SIZE = 832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [poslovnaInformatika] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [poslovnaInformatika].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [poslovnaInformatika] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [poslovnaInformatika] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [poslovnaInformatika] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [poslovnaInformatika] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [poslovnaInformatika] SET ARITHABORT OFF 
GO
ALTER DATABASE [poslovnaInformatika] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [poslovnaInformatika] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [poslovnaInformatika] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [poslovnaInformatika] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [poslovnaInformatika] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [poslovnaInformatika] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [poslovnaInformatika] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [poslovnaInformatika] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [poslovnaInformatika] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [poslovnaInformatika] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [poslovnaInformatika] SET  ENABLE_BROKER 
GO
ALTER DATABASE [poslovnaInformatika] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [poslovnaInformatika] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [poslovnaInformatika] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [poslovnaInformatika] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [poslovnaInformatika] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [poslovnaInformatika] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [poslovnaInformatika] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [poslovnaInformatika] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [poslovnaInformatika] SET  MULTI_USER 
GO
ALTER DATABASE [poslovnaInformatika] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [poslovnaInformatika] SET DB_CHAINING OFF 
GO
ALTER DATABASE [poslovnaInformatika] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [poslovnaInformatika] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [poslovnaInformatika]
GO
/****** Object:  UserDefinedDataType [dbo].[DOMAIN_1]    Script Date: 4/25/2016 5:35:01 PM ******/
CREATE TYPE [dbo].[DOMAIN_1] FROM [char](10) NULL
GO
/****** Object:  Table [dbo].[CENOVNIK]    Script Date: 4/25/2016 5:35:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CENOVNIK](
	[RBR_CENOVNIKA] [numeric](6, 0) NOT NULL,
	[DATUM_PRIMENE] [datetime] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GRUPA]    Script Date: 4/25/2016 5:35:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GRUPA](
	[GRUPA_ID] [int] NOT NULL,
	[NAZIV_GRUPE] [varchar](32) NOT NULL,
 CONSTRAINT [PK_GRUPA] PRIMARY KEY NONCLUSTERED 
(
	[GRUPA_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IZLAZNA_FAKTURA]    Script Date: 4/25/2016 5:35:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IZLAZNA_FAKTURA](
	[FA_BROJ] [numeric](6, 0) NOT NULL,
	[FA_TIP] [char](1) NOT NULL,
	[FA_DATUM] [datetime] NOT NULL,
	[FA_DATUM_VALUTE] [datetime] NOT NULL,
	[DATUM_OBRACUNA] [datetime] NULL,
	[FA_UKUPNO] [decimal](15, 2) NOT NULL,
	[FA_RABAT] [decimal](15, 2) NOT NULL,
	[FA_POREZ] [decimal](15, 2) NOT NULL,
	[FA_IZNOS] [decimal](15, 2) NOT NULL,
	[FA_TEKRACUN] [char](30) NOT NULL,
	[FA_POZIV] [varchar](20) NULL,
	[FA_STATUS] [char](1) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KATALOG_ROBE_I_USLUGA]    Script Date: 4/25/2016 5:35:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KATALOG_ROBE_I_USLUGA](
	[GRUPA_ID] [int] NOT NULL,
	[ID_ROBE] [int] NOT NULL,
	[NAZIV] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OBRACUNATI_POREZI]    Script Date: 4/25/2016 5:35:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OBRACUNATI_POREZI](
	[STOPA] [decimal](5, 2) NOT NULL,
	[IZNOS] [decimal](15, 2) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PODGRUPA]    Script Date: 4/25/2016 5:35:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PODGRUPA](
	[GRUPA_ID] [int] NOT NULL,
	[PODGRUPA_ID] [int] NOT NULL,
	[NAZIV_PODRGURPE] [varchar](32) NOT NULL,
 CONSTRAINT [PK_PODGRUPA] PRIMARY KEY CLUSTERED 
(
	[GRUPA_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PORESKA_STOPA]    Script Date: 4/25/2016 5:35:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PORESKA_STOPA](
	[IZNOS_STOPE] [decimal](5, 2) NOT NULL,
	[DATUM_VAZENJA] [datetime] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[POREZ]    Script Date: 4/25/2016 5:35:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[POREZ](
	[POR_OZNAKA] [char](2) NOT NULL,
	[POR_NAZIV] [varchar](120) NOT NULL,
	[POS_VAZI] [bit] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[POSLOVNA_GODINA]    Script Date: 4/25/2016 5:35:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[POSLOVNA_GODINA](
	[PG_GODINA] [numeric](4, 0) NOT NULL,
	[PG_VAZI] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[POSLOVNI_PARTNER]    Script Date: 4/25/2016 5:35:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[POSLOVNI_PARTNER](
	[ID_POSLOVNOGP] [int] NOT NULL,
	[TIP] [varchar](10) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PREDUZECE]    Script Date: 4/25/2016 5:35:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PREDUZECE](
	[P_Id] [int] NOT NULL,
	[NAZIV] [varchar](50) NOT NULL,
	[ADRESA] [varchar](30) NOT NULL,
	[EMAIL] [varchar](30) NULL,
	[TEKUCI_RACUN] [varchar](20) NOT NULL,
	[BANKA] [varchar](32) NOT NULL,
	[PIB] [varchar](30) NOT NULL,
	[MATICNI_BROJ] [varchar](32) NOT NULL,
	[SIFRA_DELATNOSTI] [varchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[P_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[STAVKE_CENOVNIKA]    Script Date: 4/25/2016 5:35:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[STAVKE_CENOVNIKA](
	[JEDINICNA_CENA] [decimal](10, 2) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[STAVKE_FAKTURE]    Script Date: 4/25/2016 5:35:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[STAVKE_FAKTURE](
	[REDNI_BROJ] [numeric](5, 0) NULL,
	[JEDINICA_MERE] [varchar](15) NULL,
	[KOLICINA] [decimal](15, 2) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [PRIPADA_PODGRUPI_FK]    Script Date: 4/25/2016 5:35:01 PM ******/
CREATE NONCLUSTERED INDEX [PRIPADA_PODGRUPI_FK] ON [dbo].[KATALOG_ROBE_I_USLUGA]
(
	[GRUPA_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[IZLAZNA_FAKTURA] ADD  DEFAULT ('P') FOR [FA_TIP]
GO
ALTER TABLE [dbo].[IZLAZNA_FAKTURA] ADD  DEFAULT ((0)) FOR [FA_UKUPNO]
GO
ALTER TABLE [dbo].[IZLAZNA_FAKTURA] ADD  DEFAULT ((0)) FOR [FA_RABAT]
GO
ALTER TABLE [dbo].[IZLAZNA_FAKTURA] ADD  DEFAULT ((0)) FOR [FA_POREZ]
GO
ALTER TABLE [dbo].[IZLAZNA_FAKTURA] ADD  DEFAULT ((0)) FOR [FA_IZNOS]
GO
ALTER TABLE [dbo].[IZLAZNA_FAKTURA] ADD  DEFAULT ('E') FOR [FA_STATUS]
GO
ALTER TABLE [dbo].[POREZ] ADD  DEFAULT ((1)) FOR [POS_VAZI]
GO
ALTER TABLE [dbo].[POSLOVNA_GODINA] ADD  DEFAULT ((1)) FOR [PG_VAZI]
GO
ALTER TABLE [dbo].[IZLAZNA_FAKTURA]  WITH CHECK ADD  CONSTRAINT [CKC_FA_STATUS_IZLAZNA_] CHECK  (([FA_STATUS]='S' OR [FA_STATUS]='P' OR [FA_STATUS]='O' OR [FA_STATUS]='E'))
GO
ALTER TABLE [dbo].[IZLAZNA_FAKTURA] CHECK CONSTRAINT [CKC_FA_STATUS_IZLAZNA_]
GO
ALTER TABLE [dbo].[IZLAZNA_FAKTURA]  WITH CHECK ADD  CONSTRAINT [CKC_FA_TIP_IZLAZNA_] CHECK  (([FA_TIP]='R' OR [FA_TIP]='P'))
GO
ALTER TABLE [dbo].[IZLAZNA_FAKTURA] CHECK CONSTRAINT [CKC_FA_TIP_IZLAZNA_]
GO
ALTER TABLE [dbo].[POREZ]  WITH CHECK ADD  CONSTRAINT [CKC_POS_VAZI_POREZ] CHECK  (([POS_VAZI]>=(0) AND [POS_VAZI]<=(1)))
GO
ALTER TABLE [dbo].[POREZ] CHECK CONSTRAINT [CKC_POS_VAZI_POREZ]
GO
ALTER TABLE [dbo].[POSLOVNA_GODINA]  WITH CHECK ADD  CONSTRAINT [CKC_PG_VAZI_POSLOVNA] CHECK  (([PG_VAZI]>=(0) AND [PG_VAZI]<=(1)))
GO
ALTER TABLE [dbo].[POSLOVNA_GODINA] CHECK CONSTRAINT [CKC_PG_VAZI_POSLOVNA]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'readOnly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PREDUZECE', @level2type=N'COLUMN',@level2name=N'NAZIV'
GO
USE [master]
GO
ALTER DATABASE [poslovnaInformatika] SET  READ_WRITE 
GO

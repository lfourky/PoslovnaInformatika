package app;

import javax.swing.UIManager;

import database.DBConnection;

import gui.MainFrame;

public class Application {

	public static void main(String[] args){
		DBConnection.getConnection();
		UIManager.put("OptionPane.yesButtonText", "Da");
		UIManager.put("OptionPane.noButtonText", "Ne");
		UIManager.put("OptionPane.cancelButtonText", "Otkaži");
		MainFrame mf = new MainFrame();
		mf.setVisible(true);
	}
}

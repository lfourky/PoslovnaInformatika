package database;

import database.query.GenericTableModel;

public class CatalogItemTableModel extends GenericTableModel{
	private static final long serialVersionUID = -5138061893076854345L;

	public CatalogItemTableModel(String[] column_names, int rowCount, String table_name, String id_column_name) {
		super(column_names, rowCount, table_name, id_column_name);
	}
}

package database.query;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import model.bean.Data;

public interface CRUD {

	public int updateRow(LinkedList<Data> values, int index) throws SQLException;

	public int insertRow(LinkedList<Data> values) throws SQLException;

	public void deleteRow(int index) throws SQLException;

	public void checkRow(int index) throws SQLException;

}

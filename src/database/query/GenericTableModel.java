package database.query;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.swing.table.DefaultTableModel;

import database.DBConnection;
import gui.dialog.SortUtils;
import model.bean.Data;
import util.Utils;

public abstract class GenericTableModel extends DefaultTableModel implements CRUD{

	private static final long serialVersionUID = -961216162767064635L;

	private String table_name;
	private Object[] column_names;
	private String id_column_name;
	
	private static final String ERROR_RECORD_WAS_DELETED = "Record was deleted!";
	private static final String ERROR_RECORD_WAS_CHANGED = "Record was changed!";

	public GenericTableModel(String[] column_names, int rowCount, String table_name, String id_column_name) {
		super(column_names, rowCount);
		this.table_name = table_name;
		this.id_column_name = id_column_name;
		this.column_names = column_names;
	}

	public void open() throws SQLException {
		fillData(QueryBuilder.select_all(table_name));
	}
	
	public void fillData(String sql) throws SQLException {
		setRowCount(0);
		Statement stmt = DBConnection.getConnection().createStatement();
		ResultSet rset = stmt.executeQuery(sql);
		while (rset.next()) {
			ArrayList<String> values = new ArrayList<String>();
			for(int i=0; i<column_names.length; i++)
				values.add(String.valueOf(rset.getObject(String.valueOf(column_names[i]))));	
			addRow(Utils.listToArray(values));
		}
		
		rset.close();
		stmt.close();
		fireTableDataChanged();
	}
	
	@Override
	public int updateRow(LinkedList<Data> values, int index)  throws SQLException {
		int retVal = 0;
		Integer selected_item_id = new Integer((String) getValueAt(index, 0));
		PreparedStatement stmt = DBConnection.getConnection().prepareStatement(QueryBuilder.update(values, table_name, id_column_name));
		System.out.println(QueryBuilder.update(values, table_name, id_column_name));
		
		int j = 1;
		for (Data d : values) 
			stmt.setObject(j++, d.getValue());
		stmt.setInt(j, selected_item_id);
		int rowsAffected = stmt.executeUpdate();
		stmt.close();
		// Unos sloga u bazu
		DBConnection.getConnection().commit();
		if (rowsAffected > 0) {
			int k = 1;
			for (Data d : values)
				setValueAt(d.getValue(), index, k++);
			fireTableDataChanged();
		}
		return retVal;
	}

	@Override
	public int insertRow(LinkedList<Data> values) throws SQLException {
		int retVal = 0;
		PreparedStatement stmt = DBConnection.getConnection().prepareStatement(QueryBuilder.insert(values, table_name), Statement.RETURN_GENERATED_KEYS);
		for(int i=1; i<=values.size(); i++){
			Data data = values.get(i-1);
			stmt.setObject(i, data.getValue());
		}
		int rowsAffected = stmt.executeUpdate();
		DBConnection.getConnection().commit();
		
		if (rowsAffected > 0) {
			ResultSet rs = stmt.getGeneratedKeys();
			while (rs.next()) {	
				Integer id = rs.getInt(1);
				retVal = id; //sortedInsert(id, values);
				fireTableDataChanged();
				System.out.println("USPESNO SMO UPDATETOVALI BAZU :)");
			}
		}
		stmt.close();
		return retVal;
	}
	
	/*private int sortedInsert(Integer id, LinkedList<Data> values){
		int left = 0;
		int right = getRowCount() - 1;
		int mid = (left + right) / 2;
		while (left <= right) {
			mid = (left + right) / 2;
			Integer an_id = Integer.parseInt((String) getValueAt(mid, 0));
			if (SortUtils.getLatCyrCollator().compare(id, an_id) > 0)
				left = mid + 1;
			else if (SortUtils.getLatCyrCollator().compare(id.toString(), an_id) < 0)
				right = mid - 1;
			else
				break;
		}
		insertRow(left, Utils.linkedListToArray(id, values));
		return left;
	}*/


	@Override
	public void deleteRow(int index) throws SQLException {
		checkRow(index);
		PreparedStatement stmt = DBConnection.getConnection().prepareStatement(QueryBuilder.delete(table_name, id_column_name));
		String old_id = (String) getValueAt(index, 0);
		stmt.setString(1, old_id);
		int rowsAffected = stmt.executeUpdate();
		stmt.close();
		DBConnection.getConnection().commit();
		if (rowsAffected > 0) {
			removeRow(index);
			fireTableDataChanged();
		}
	}

	@Override
	public void checkRow(int index) throws SQLException {
		DBConnection.getConnection().setTransactionIsolation(Connection.TRANSACTION_REPEATABLE_READ);
		PreparedStatement selectStmt = DBConnection.getConnection().prepareStatement(QueryBuilder.select_all(table_name) + " where " + id_column_name + "=?");

		String sifra = (String) getValueAt(index, 0);
		selectStmt.setString(1, sifra);

		ResultSet rset = selectStmt.executeQuery();

		String id = "";
		Boolean exists = false;
		String errorMsg = "";
		while (rset.next()) {
			id = rset.getString(id_column_name).trim();
			exists = true;
		}
		if (!exists) {
			removeRow(index);
			fireTableDataChanged();
			errorMsg = ERROR_RECORD_WAS_DELETED;
		} else if ((SortUtils.getLatCyrCollator().compare(id, ((String) getValueAt(index, 0)).trim()) != 0)) {
			for (int i = 0; i < column_names.length; i++) 
				setValueAt("", index, i);
			fireTableDataChanged();
			errorMsg = ERROR_RECORD_WAS_CHANGED;
		}
		rset.close();
		selectStmt.close();
		DBConnection.getConnection().setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
		if (errorMsg != "") {
			DBConnection.getConnection().commit();
			throw new SQLException(errorMsg, "", 500);
		}
	}
	

}

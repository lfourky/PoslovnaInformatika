package database.query;

import java.util.LinkedList;

import model.bean.Data;

public class QueryBuilder {

	public static String insert(LinkedList<Data> values, String table_name) {
		return "INSERT INTO " + table_name + " " + insert_parameters(values) + " VALUES " + question_marks(values.size());
	}

	public static String select_all(String table_name) {
		return "SELECT * FROM " + table_name;
	}

	public static String update(LinkedList<Data> values, String table_name, String id_column_name) {
		return "UPDATE " + table_name + " SET " + update_parameters(values) + " WHERE " + id_column_name + "=?";
	}

	public static String delete(String table_name, String id_name) {
		return "DELETE FROM " + table_name + " WHERE " + id_name + "= ?";
	}

	private static String update_parameters(LinkedList<Data> values) {
		String value_names = "";
		for (int i = 0; i < values.size(); i++)
			if (i == values.size() - 1)
				value_names += values.get(i).getName() + "=?";
			else
				value_names += values.get(i).getName() + "=?, ";

		return value_names;
	}

	private static String insert_parameters(LinkedList<Data> values) {
		String value_names = "(";
		for (int i = 0; i < values.size(); i++)
			if (i == values.size() - 1)
				value_names += values.get(i).getName() + ")";
			else
				value_names += values.get(i).getName() + ", ";

		return value_names;
	}

	private static String question_marks(int number_of_question_marks) {
		String sp = "(";
		for (int i = 0; i < number_of_question_marks; i++)
			if (i == number_of_question_marks - 1)
				sp += "?)";
			else
				sp += "?,";
		return sp;
	}
}

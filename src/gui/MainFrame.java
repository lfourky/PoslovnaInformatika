package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import database.DBConnection;
import gui.dialog.BusinessPartnerStandardForm;
import gui.dialog.CatalogItemStandardForm;
import gui.dialog.CatalogStandardForm;
import gui.dialog.CompanyStandardForm;
import gui.dialog.GroupStandardForm;
import gui.dialog.SubgroupStandardForm;
import gui.dialog.TaxRateStandardForm;
import gui.dialog.TaxStandardForm;

public class MainFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static MainFrame instance;

	public MainFrame() {

		setTitle("Projekat 2015");
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		// setExtendedState(MAXIMIZED_BOTH);
		setSize(800, 600);
		setLocationRelativeTo(null);

		initMenu();

		addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent arg0) {
				// if (JOptionPane.showConfirmDialog(null, "Da li ste sigurni da
				// želite da zatvorite aplikaciju?", "Potvrda",
				// JOptionPane.YES_NO_OPTION)
				// == JOptionPane.YES_OPTION){
				DBConnection.close();
				System.exit(0);
				// }

			}
		});

	}

	private void initMenu() {
		JMenuBar menuBar = new JMenuBar();
		JMenu orgSemaMenu = new JMenu("Faktura");
		JMenuItem company = new JMenuItem("Company");
		JMenuItem grupe = new JMenuItem("Group");
		JMenuItem podgrupe = new JMenuItem("Subgroup");
		JMenuItem poslovniPartner = new JMenuItem("Poslovni Partner");
		JMenuItem tax = new JMenuItem("Tax");
		JMenuItem rate = new JMenuItem("Tax rate");
		JMenuItem catalog = new JMenuItem("Catalog");
		JMenuItem ci = new JMenuItem("Catalog item");

		orgSemaMenu.add(company);
		orgSemaMenu.add(grupe);
		orgSemaMenu.add(podgrupe);
		orgSemaMenu.add(poslovniPartner);
		orgSemaMenu.add (tax);
		orgSemaMenu.add (rate);
		orgSemaMenu.add (catalog);
		orgSemaMenu.add (ci);
		menuBar.add(orgSemaMenu);
		setJMenuBar(menuBar);

		company.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				CompanyStandardForm drzavaSF = new CompanyStandardForm();
				drzavaSF.setVisible(true);

			}
		});
		
		ci.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				CatalogItemStandardForm cisf = new CatalogItemStandardForm(null);
				cisf.setVisible(true);

			}
		});
		
		catalog.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				CatalogStandardForm csf = new CatalogStandardForm();
				csf.setVisible(true);

			}
		});
		
		rate.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				TaxRateStandardForm trsf = new TaxRateStandardForm(null);
				trsf.setVisible(true);

			}
		});

		tax.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				TaxStandardForm tsf = new TaxStandardForm();
				tsf.setVisible(true);

			}
		});

		poslovniPartner.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				BusinessPartnerStandardForm bp = new BusinessPartnerStandardForm(null);
				bp.setVisible(true);

			}
		});

		podgrupe.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				SubgroupStandardForm psf = new SubgroupStandardForm(null);
				psf.setVisible(true);
			}
		});

		grupe.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				GroupStandardForm gsf = new GroupStandardForm();
				gsf.setVisible(true);
			}
		});

	}

	public static MainFrame getInstance() {
		if (instance == null)
			instance = new MainFrame();
		return instance;
	}

}

package gui.dialog;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import annotation.util.AnnotationHelper;
import database.CatalogTableModel;
import database.CompanyTableModel;
import database.GroupTableModel;
import gui.MainFrame;
import model.bean.Catalog;
import model.bean.Company;
import model.bean.Data;
import model.bean.Group;
import net.miginfocom.swing.MigLayout;
import util.Column;
import util.ColumnList;
import util.Utils;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Color;

public class CatalogStandardForm extends JDialog {
	private static final long serialVersionUID = 1L;
	
	//Constants
	private static final int MODE_EDIT = 1;
	private static final int MODE_ADD = 2;
	private static final int MODE_SEARCH = 3;
	
	private int mode;
	public ColumnList columnLista;

	private JToolBar toolBar;
	
	//Buttons
	private JButton btnAdd, btnCommit, btnDelete, btnFirst, btnLast, btnHelp,
			btnNext, btnNextForm, btnPickup, btnRefresh, btnRollback,
			btnSearch, btnPrevious;
	
	private JTable tblGrid;
	public CatalogTableModel tableModel;
	
	//TextField
	private JTextField tdIdCatalog = new JTextField(20);
	private JTextField tfActingDate = new JTextField(20);
	private JLabel labelMode;
	
	private JLabel lbGrupa = new JLabel("Catalog ID:");
	private JLabel lbNaziv = new JLabel("Acting date:");

	public CatalogStandardForm() {

		getContentPane().setLayout(new MigLayout("fill", "[]", "[][][][][]"));
		setSize(new Dimension(800, 600));
		setTitle("Grupe");
		setLocationRelativeTo(MainFrame.getInstance());
		setModal(true);
		initToolbar();
		initTable();
		initGui();
		setMode(MODE_EDIT);
		labelMode.setText("EDIT");
		tdIdCatalog.setVisible(true);
		lbGrupa.setVisible(true);
		tdIdCatalog.setEditable(false);
		tblGrid.requestFocus();
		tblGrid.changeSelection(0,0,false, false);
	}

	private void initGui() {
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new MigLayout("fillx", "[]", "[]"));
		JPanel dataPanel = new JPanel();
		dataPanel.setLayout(new MigLayout("gapx 15px", "[][][][grow][][grow][][grow][][grow]", "[][][]"));

		JPanel buttonsPanel = new JPanel();
		btnCommit = new JButton(new ImageIcon(getClass().getResource(
				"/img/commit.gif")));
		btnCommit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (mode == MODE_EDIT) {

					LinkedList<Data> values = new LinkedList<Data>();
					values.add(new Data("acting_date", getTFText(tfActingDate)));
					int selected = tblGrid.getSelectedRow();
					try {
						
						tableModel.updateRow(values, selected);
						
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					tblGrid.setRowSelectionInterval(selected, selected);
				} else if (mode == MODE_ADD) {
					addRow();
				} else if (mode == MODE_SEARCH) {
					//tableModel.search(tfIdGrupe.getText(), tfNazivGrupe.getText());
					setMode(MODE_EDIT);
				}

			}
			private String getTFText(JTextField tf) {
				return tf.getText().trim();
			}
		});
		btnRollback = new JButton(new ImageIcon(getClass().getResource(
				"/img/remove.gif")));
		btnRollback.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

			}
		});
		
		labelMode = new JLabel("");
		labelMode.setForeground(Color.DARK_GRAY);
		labelMode.setFont(new Font("Tahoma", Font.BOLD, 30));
		labelMode.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(labelMode, "cell 0 1,alignx center");

	

		dataPanel.add(lbGrupa, "cell 0 1");
		dataPanel.add(tdIdCatalog, "cell 1 1");
		dataPanel.add(lbNaziv, "cell 0 2");
		dataPanel.add(tfActingDate, "cell 1 2");
		bottomPanel.add(dataPanel, "cell 0 0");

		buttonsPanel.setLayout(new MigLayout("wrap"));
		buttonsPanel.add(btnCommit);
		buttonsPanel.add(btnRollback);
		bottomPanel.add(buttonsPanel, "dock east");

		getContentPane().add(bottomPanel, "cell 0 3,grow");
	}

	private void sync() {
		int index = tblGrid.getSelectedRow();
		if (index < 0) {
			tdIdCatalog.setText("");
			tfActingDate.setText("");
			return;
		}
		String sifra = (String) tableModel.getValueAt(index, 0);
		String naziv = (String) tableModel.getValueAt(index, 1);
		tdIdCatalog.setText(sifra);
		tfActingDate.setText(naziv);
		setMode(MODE_EDIT);
		labelMode.setText("EDIT");
		tdIdCatalog.setVisible(true);
		lbGrupa.setVisible(true);
		tdIdCatalog.setEditable(false);
	}

	private void goLast() {
		int rowCount = tblGrid.getModel().getRowCount();
		if (rowCount > 0)
			tblGrid.setRowSelectionInterval(rowCount - 1, rowCount - 1);
	}

	private void initTable() {
		tblGrid = new JTable();
		JScrollPane scrollPane = new JScrollPane(tblGrid);
		getContentPane().add(scrollPane, "cell 0 0,grow");
		// Kreiranje TableModel-a, parametri: header-i kolona i broj redova
		String[] column_names = AnnotationHelper.getColumnNames(Catalog.class);
		String table_name = AnnotationHelper.getTableName(Catalog.class);
		String id_column_name = AnnotationHelper.getId(Catalog.class);
		tableModel = new CatalogTableModel(column_names, 0, table_name, id_column_name);
		tblGrid.setModel(tableModel);
		try {
			tableModel.open();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Dozvoljeno selektovanje redova
		tblGrid.setRowSelectionAllowed(true);
		// Ali ne i selektovanje kolona
		tblGrid.setColumnSelectionAllowed(false);
		// Dozvoljeno selektovanje samo jednog reda u jedinici vremena
		tblGrid.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblGrid.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent e) {
						if (e.getValueIsAdjusting())
							return;
						sync();
					}
				});

	}

	private void initToolbar() {
		toolBar = new JToolBar();

		btnSearch = new JButton(new ImageIcon(getClass().getResource(
				"/img/search.gif")));

		btnSearch.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setMode(MODE_SEARCH);
				tdIdCatalog.setText("");
				tfActingDate.setText("");

			}
		});

		toolBar.add(btnSearch);

		btnRefresh = new JButton(new ImageIcon(getClass().getResource(
				"/img/refresh.gif")));

		btnRefresh.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String s = "SELECT * FROM GRUPA";
				try {
					tableModel.fillData(s);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});

		toolBar.add(btnRefresh);

		btnPickup = new JButton(new ImageIcon(getClass().getResource(
				"/img/zoom-pickup.gif")));
		btnPickup.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				Column sifra = new Column();
				Column naziv = new Column();
				columnLista = new ColumnList();
				int index = tblGrid.getSelectedRow();

				if (!(index < 0)) {
					String sifra1 = tdIdCatalog.getText();
					String naziv1 = tfActingDate.getText();
					sifra.setName("id");
					sifra.setValue(sifra1);
					naziv.setName("name");
					naziv.setValue(naziv1);
					columnLista.add(naziv);
					columnLista.add(sifra);

				}
				setVisible(false);

			}
		});

		toolBar.add(btnPickup);

		btnHelp = new JButton(new ImageIcon(getClass().getResource(
				"/img/help.gif")));
		btnHelp.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		toolBar.add(btnHelp);

		toolBar.addSeparator();

		btnFirst = new JButton(new ImageIcon(getClass().getResource(
				"/img/first.gif")));
		btnFirst.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				goFirst();

			}
		});

		toolBar.add(btnFirst);

		btnPrevious = new JButton(new ImageIcon(getClass().getResource(
				"/img/prev.gif")));
		btnPrevious.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				goPrevious();

			}
		});
		toolBar.add(btnPrevious);

		btnNext = new JButton(new ImageIcon(getClass().getResource(
				"/img/next.gif")));
		btnNext.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				goNext();
			}
		});
		toolBar.add(btnNext);

		btnLast = new JButton(new ImageIcon(getClass().getResource(
				"/img/last.gif")));
		btnLast.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				goLast();
			}
		});
		toolBar.add(btnLast);

		toolBar.addSeparator();

		btnAdd = new JButton(new ImageIcon(getClass().getResource(
				"/img/add.gif")));
		btnAdd.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setMode(MODE_ADD);
				tdIdCatalog.setVisible(false);
				lbGrupa.setVisible(false);
				labelMode.setText("ADD");
				tfActingDate.setText("");
				tdIdCatalog.setText("");
				tfActingDate.requestFocus();

			}
		});
		toolBar.add(btnAdd);

		btnDelete = new JButton(new ImageIcon(getClass().getResource(
				"/img/remove.gif")));
		btnDelete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog.setDefaultLookAndFeelDecorated(true);
				int response = JOptionPane
						.showConfirmDialog(null, "Do you want to continue?",
								"Confirm", JOptionPane.YES_NO_OPTION,
								JOptionPane.QUESTION_MESSAGE);
				if (response == JOptionPane.NO_OPTION) {

				} else if (response == JOptionPane.YES_OPTION) {
					int index= tblGrid.getSelectedRow();
					try {
						tableModel.deleteRow(index);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} else if (response == JOptionPane.CLOSED_OPTION) {

				}

			}
		});
		toolBar.add(btnDelete);

		toolBar.addSeparator();

		btnNextForm = new JButton(new ImageIcon(getClass().getResource(
				"/img/nextform.gif")));
		btnNextForm.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
	
				Column sifra = new Column();
				Column naziv = new Column();
				columnLista = new ColumnList();
				//int index = tblGrid.getSelectedRow();
					String sifra1 = tdIdCatalog.getText();
					String naziv1 = tfActingDate.getText();
					sifra.setName("podgrupa.podgrupa_id");
					sifra.setValue(sifra1);
					naziv.setName("podgrupa.naziv_podrgurpe");
					naziv.setValue(naziv1);
					columnLista.add(naziv);
					columnLista.add(sifra);

				SubgroupStandardForm nm;
				nm = new SubgroupStandardForm(columnLista);
				nm.setVisible(true);
				
				
				

			}
		});
		toolBar.add(btnNextForm);

		getContentPane().add(toolBar, "dock north");

	}


	

	private void addRow() {
		LinkedList<Data> values= new LinkedList<Data>();
		String nazivGrupe = tfActingDate.getText().trim();
		values.add(new Data("acting_date", nazivGrupe));
	
	try {
		int index = tableModel.insertRow(values);
		tableModel.insertRow(tableModel.getRowCount(), Utils.linkedListToArray(index, values));
			setMode(MODE_ADD);
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(this, ex.getMessage(), "Greska",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	private void goFirst() {
		int rowCount = tblGrid.getModel().getRowCount();
		if (rowCount > 0)
			tblGrid.setRowSelectionInterval(0, 0);
	}

	private void goNext() {
		int rowCount = tblGrid.getModel().getRowCount();
		int selected = tblGrid.getSelectedRow();

		if (selected == -1)
			return;

		int next = selected < rowCount - 1 ? selected + 1 : 0;
		tblGrid.setRowSelectionInterval(next, next);

	}

	private void goPrevious() {
		int rowCount = tblGrid.getModel().getRowCount();
		int selected = tblGrid.getSelectedRow();

		if (selected == -1)
			return;

		int previous = selected > 0 ? selected - 1 : rowCount - 1;
		tblGrid.setRowSelectionInterval(previous, previous);
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	public ColumnList getColumnLista() {
		return columnLista;
	}

	public void setColumnLista(ColumnList columnLista) {
		this.columnLista = columnLista;
	}
//test
}

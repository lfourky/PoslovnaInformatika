package gui.dialog;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.LinkedList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import annotation.util.AnnotationHelper;
import database.CompanyTableModel;
import database.query.QueryBuilder;
import gui.MainFrame;
import model.bean.Company;
import model.bean.Data;
import net.miginfocom.swing.MigLayout;
import util.Column;
import util.ColumnList;
import util.Utils;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;

public class CompanyStandardForm extends JDialog {
	private static final long serialVersionUID = 1L;
	
	//Constants
	private static final int MODE_EDIT = 1;
	private static final int MODE_ADD = 2;
	private static final int MODE_SEARCH = 3;
	
	private int mode;
	public ColumnList columnLista;

	private JToolBar toolBar;
	
	//Buttons
	private JButton btnAdd, btnCommit, btnDelete, btnFirst, btnLast, btnHelp, btnNext, btnNextForm,
	                btnPickup, btnRefresh, btnRollback, btnSearch, btnPrevious;
	
	private JTable tblGrid;
	public CompanyTableModel tableModel;
	
	//Label
	private JLabel label_id = new JLabel("ID:");
	private JLabel label_name = new JLabel("Name:");
	private JLabel label_address = new JLabel("Address: ");
	private JLabel label_bankaccount = new JLabel("BA:");
	private JLabel label_bank = new JLabel("Bank:");
	private JLabel label_email = new JLabel("Email:");
	private JLabel label_companynumber = new JLabel("CN:");
	private JLabel label_activitycode = new JLabel("AC");
	private JLabel label_TIN = new JLabel("TIN");
	
	//TextField
	private JTextField textfield_id;
	private JTextField textfield_name;
	private JTextField textfield_address;
	private JTextField textfield_email;
	private JTextField textfield_bankaccount;
	private JTextField textfield_bank;
	private JTextField textfield_TIN;
	private JTextField textfield_companynumber;
	private JTextField textfield_activitycode;
	private JLabel labelMode;
	
	

	public CompanyStandardForm() {

		getContentPane().setLayout(new MigLayout("fill", "[]", "[][][][]"));

		setSize(new Dimension(800, 600));
		setTitle("Company");
		setLocationRelativeTo(MainFrame.getInstance());
		setModal(true);
		initToolbar();
		initTable();
		initGui();
		setMode(MODE_EDIT);
		labelMode.setText("EDIT");
		textfield_id.setVisible(true);
		label_id.setVisible(true);
		textfield_id.setEditable(false);
		tblGrid.requestFocus();
		tblGrid.changeSelection(0,0,false, false);
	}

	private void initGui() {
		
		JPanel bottomPanel = new JPanel();
		JPanel buttonsPanel = new JPanel();
		JPanel dataPanel = new JPanel();
		
		bottomPanel.add(dataPanel, "cell 0 0");
		dataPanel.setLayout(new MigLayout("gapx 15px", "[][][][grow][][grow][][grow][][grow]", "[][][]"));
		bottomPanel.setLayout(new MigLayout("fillx", "[]", "[]"));
		
		textfield_id            = new JTextField(10);
		textfield_name          = new JTextField(10);
		textfield_address       = new JTextField(10);
		textfield_bankaccount   = new JTextField(10);
		textfield_companynumber = new JTextField(10);
		textfield_TIN 		    = new JTextField(10);
		textfield_email 		= new JTextField(10);
		textfield_bank          = new JTextField(10);
		textfield_activitycode  = new JTextField(10);

		dataPanel.add(label_id, "cell 0 1");
		dataPanel.add(label_address, "cell 2 1,alignx trailing");
		dataPanel.add(label_bankaccount, "cell 4 1,alignx trailing");
		dataPanel.add(label_companynumber, "cell 6 1,alignx trailing");
		dataPanel.add(label_TIN, "cell 8 1,alignx trailing");
		dataPanel.add(label_name, "cell 0 2");
		dataPanel.add(label_email, "cell 2 2,alignx trailing");
		dataPanel.add(label_bank, "cell 4 2,alignx trailing");
		dataPanel.add(label_activitycode, "cell 6 2,alignx trailing");
		
		textfield_id.setEditable(false);
		
		dataPanel.add(textfield_id, "cell 1 1");
		dataPanel.add(textfield_name, "cell 1 2");
		dataPanel.add(textfield_address, "cell 3 1,growx");
		dataPanel.add(textfield_bankaccount, "cell 5 1,growx");
		dataPanel.add(textfield_companynumber, "cell 7 1,growx");
		dataPanel.add(textfield_TIN, "cell 9 1,growx");
		dataPanel.add(textfield_email, "cell 3 2,growx");
		dataPanel.add(textfield_bank, "cell 5 2,growx");
		dataPanel.add(textfield_activitycode, "cell 7 2,growx");

		
		/**BUTTONS*/	
		btnCommit = new JButton(new ImageIcon(getClass().getResource("/img/commit.gif")));
		btnCommit.addActionListener(commitAction());
		
		labelMode = new JLabel("");
		labelMode.setBackground(Color.WHITE);
		labelMode.setFont(new Font("Tahoma", Font.BOLD, 30));
		labelMode.setForeground(Color.DARK_GRAY);
		labelMode.setVerticalAlignment(SwingConstants.TOP);
		getContentPane().add(labelMode, "cell 0 1,alignx center");
		btnRollback = new JButton(new ImageIcon(getClass().getResource("/img/remove.gif")));
		
		buttonsPanel.setLayout(new MigLayout("wrap"));
		buttonsPanel.add(btnCommit);
		buttonsPanel.add(btnRollback);
		bottomPanel.add(buttonsPanel, "dock east");

		getContentPane().add(bottomPanel, "cell 0 2,grow");
	}

	private ActionListener commitAction() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (mode == MODE_EDIT) {	
					int selected = tblGrid.getSelectedRow();
					try {
						LinkedList<Data> values = new LinkedList<Data>();
						values.add(new Data("name", getTFText(textfield_name)));
						values.add(new Data("address", getTFText(textfield_address)));
						values.add(new Data("email", getTFText(textfield_email)));
						values.add(new Data("bank_account", getTFText(textfield_bankaccount)));
						values.add(new Data("bank", getTFText(textfield_bank) ));
						values.add(new Data("TIN", getTFText(textfield_TIN)));
						values.add(new Data("company_number", getTFText(textfield_companynumber)));
						values.add(new Data("activity_code", getTFText(textfield_activitycode)));
						tableModel.updateRow(values, selected);
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					tblGrid.setRowSelectionInterval(selected, selected); // TODO: check this
				} else if (mode == MODE_ADD) 
					addRow();
				 else if (mode == MODE_SEARCH) {
					//tableModel.search(tfSifra.getText(), tfName.getText()); //TODO commented out cuz idk what does it do
					//setMode(MODE_EDIT);
				}

			}

			private String getTFText(JTextField tf) {
				return tf.getText().trim();
			}
		};
	}

	private void sync() {
		int index = tblGrid.getSelectedRow();
		if (index < 0) {
			textfield_id.setText("");
			textfield_name.setText("");
			return;
		}
		String sifra = (String) tableModel.getValueAt(index, 0);
		String naziv = (String) tableModel.getValueAt(index, 1);
		String adresa = (String) tableModel.getValueAt(index, 2) ;
		String email = (String) tableModel.getValueAt(index, 3) ;
		String tr = (String) tableModel.getValueAt(index, 4);
		String banka = (String) tableModel.getValueAt(index, 5);
		String pib = (String) tableModel.getValueAt(index, 6);
		String mb = (String) tableModel.getValueAt(index, 7);
		String sd = (String) tableModel.getValueAt(index, 8) ;
		textfield_id.setText(sifra);
		textfield_name.setText(naziv);
		textfield_address.setText(adresa);
		textfield_email.setText(email);
		textfield_bankaccount.setText(tr);
		textfield_bank.setText(banka);
		textfield_TIN.setText(pib);
		textfield_companynumber.setText(mb);
		textfield_activitycode.setText(sd);
		setMode(MODE_EDIT);
		labelMode.setText("EDIT");
		textfield_id.setVisible(true);
		label_id.setVisible(true);
		textfield_id.setEditable(false);
	}

	private void goLast() {
		int rowCount = tblGrid.getModel().getRowCount();
		if (rowCount > 0)
			tblGrid.setRowSelectionInterval(rowCount - 1, rowCount - 1);
	}

	private void initTable() {
		tblGrid = new JTable();
		JScrollPane scrollPane = new JScrollPane(tblGrid);
		getContentPane().add(scrollPane, "cell 0 0,grow");
		// Kreiranje TableModel-a, parametri: header-i kolona i broj redova
		String[] column_names = AnnotationHelper.getColumnNames(Company.class);
		String table_name = AnnotationHelper.getTableName(Company.class);
		String id_column_name = AnnotationHelper.getId(Company.class);
		tableModel = new CompanyTableModel(column_names, 0, table_name, id_column_name);
		tblGrid.setModel(tableModel);
		try {
			tableModel.open();
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// Dozvoljeno selektovanje redova
		tblGrid.setRowSelectionAllowed(true);
		// Ali ne i selektovanje kolona
		tblGrid.setColumnSelectionAllowed(false);
		// Dozvoljeno selektovanje samo jednog reda u jedinici vremena
		tblGrid.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblGrid.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent e) {
						if (e.getValueIsAdjusting())
							return;
						sync();
					}
				});
	}

	private void initToolbar() {
		toolBar = new JToolBar();

		btnSearch = new JButton(new ImageIcon(getClass().getResource(
				"/img/search.gif")));

		// TODO
		/*
		 * U listenerima pozvati odgovarajuce metode forme, poput goLast,
		 * goFirst, goNext, goPrevious Dodati metode za dodavanje, pretragu,
		 * brisanje... Bolje je izdvojiti kod u posebne metode pa pozvati iz
		 * listenra, nego direktno staviti u listener Ovako mozemo vise puta
		 * iskoristiti dati kod pozivanjem metode sa vise mesta, sto nije moguce
		 * ako ga stavimo u listener
		 */

		btnSearch.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setMode(MODE_SEARCH);
				textfield_id.setText("");
				textfield_name.setText("");

			}
		});

		toolBar.add(btnSearch);

		btnRefresh = new JButton(new ImageIcon(getClass().getResource(
				"/img/refresh.gif")));

		btnRefresh.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					tableModel.fillData(QueryBuilder.select_all("company"));
				} catch (SQLException e1) {
					e1.printStackTrace();
				}

			}
		});

		toolBar.add(btnRefresh);

		btnPickup = new JButton(new ImageIcon(getClass().getResource(
				"/img/zoom-pickup.gif")));
		btnPickup.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				Column sifra = new Column();
				Column naziv = new Column();
				columnLista = new ColumnList();
				int index = tblGrid.getSelectedRow();

				if (!(index < 0)) {
					String sifra1 = textfield_id.getText();
					String naziv1 = textfield_name.getText();
					sifra.setName("ID");
					sifra.setValue(sifra1);
					naziv.setName("name");
					naziv.setValue(naziv1);
					columnLista.add(naziv);
					columnLista.add(sifra);

				}
				setVisible(false);

			}
		});

		toolBar.add(btnPickup);

		btnHelp = new JButton(new ImageIcon(getClass().getResource(
				"/img/help.gif")));
		btnHelp.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

			}
		});

		toolBar.add(btnHelp);

		toolBar.addSeparator();

		btnFirst = new JButton(new ImageIcon(getClass().getResource(
				"/img/first.gif")));
		btnFirst.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				goFirst();

			}
		});

		toolBar.add(btnFirst);

		btnPrevious = new JButton(new ImageIcon(getClass().getResource(
				"/img/prev.gif")));
		btnPrevious.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				goPrevious();

			}
		});
		toolBar.add(btnPrevious);

		btnNext = new JButton(new ImageIcon(getClass().getResource(
				"/img/next.gif")));
		btnNext.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				goNext();
			}
		});
		toolBar.add(btnNext);

		btnLast = new JButton(new ImageIcon(getClass().getResource(
				"/img/last.gif")));
		btnLast.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				goLast();
			}
		});
		toolBar.add(btnLast);

		toolBar.addSeparator();

		btnAdd = new JButton(new ImageIcon(getClass().getResource(
				"/img/add.gif")));
		btnAdd.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setMode(MODE_ADD);
				textfield_id.setVisible(false);
				label_id.setVisible(false);
				textfield_id.setEditable(true);
				labelMode.setText("ADD");
				textfield_name.setText("");
				textfield_id.setText("");
				textfield_address.setText("");
				textfield_email.setText("");
				textfield_bankaccount.setText("");
				textfield_bank.setText("");
				textfield_TIN.setText("");
				textfield_companynumber.setText("");
				textfield_activitycode.setText("");
				textfield_name.requestFocus();

			}
		});
		toolBar.add(btnAdd);

		btnDelete = new JButton(new ImageIcon(getClass().getResource(
				"/img/remove.gif")));
		btnDelete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog.setDefaultLookAndFeelDecorated(true);
				int response = JOptionPane
						.showConfirmDialog(null, "Do you want to continue?",
								"Confirm", JOptionPane.YES_NO_OPTION,
								JOptionPane.QUESTION_MESSAGE);
				if (response == JOptionPane.NO_OPTION) {

				} else if (response == JOptionPane.YES_OPTION) {
					int selected = tblGrid.getSelectedRow();
					try {
						tableModel.deleteRow(selected);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} else if (response == JOptionPane.CLOSED_OPTION) {

				}

			}
		});
		toolBar.add(btnDelete);

		toolBar.addSeparator();

		btnNextForm = new JButton(new ImageIcon(getClass().getResource(
				"/img/nextform.gif")));
		btnNextForm.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
	
				Column sifra = new Column();
				Column naziv = new Column();
				columnLista = new ColumnList();
				//int index = tblGrid.getSelectedRow();
					String sifra1 = textfield_id.getText();
					String naziv1 = textfield_name.getText();
					sifra.setName("NASELJENO_MESTO.dr_sifra");
					sifra.setValue(sifra1);
					naziv.setName("dr_naziv");
					naziv.setValue(naziv1);
					columnLista.add(naziv);
					columnLista.add(sifra);

			//	NaseljenoMestoStandardForm nm;
				//nm = new NaseljenoMestoStandardForm(columnLista);
				//nm.setVisible(true);
				
				
				

			}
		});
		toolBar.add(btnNextForm);

		getContentPane().add(toolBar, "dock north");

	}


	private void addRow() {
		String naziv = textfield_name.getText().trim();
		String adresa = textfield_address.getText().trim();
		String email = textfield_email.getText().trim();
		String tekuciRacun = textfield_bankaccount.getText().trim();
		String banka = textfield_bank.getText().trim();
		String pib = textfield_TIN.getText().trim();
		String maticni = textfield_companynumber.getText().trim();
		String sifraDelatnosti = textfield_activitycode.getText().trim();
		LinkedList<Data> values= new LinkedList<Data>();
		values.add(new Data("name", naziv));
		values.add(new Data("email", email));
		values.add(new Data("bank_account", tekuciRacun));
		values.add(new Data("address", adresa));
		values.add(new Data("bank", banka ));
		values.add(new Data("TIN", pib));
		values.add(new Data("company_number", maticni));
		values.add(new Data("activity_code", sifraDelatnosti));
		try {
			int index = tableModel.insertRow(values);
			tableModel.insertRow(tableModel.getRowCount(), Utils.linkedListToArray(index, values));
			tblGrid.requestFocus();
			tblGrid.changeSelection(tableModel.getRowCount()-1,tableModel.getRowCount()-1,false, false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	private void goFirst() {
		int rowCount = tblGrid.getModel().getRowCount();
		if (rowCount > 0)
			tblGrid.setRowSelectionInterval(0, 0);
	}

	private void goNext() {
		int rowCount = tblGrid.getModel().getRowCount();
		int selected = tblGrid.getSelectedRow();

		if (selected == -1)
			return;

		int next = selected < rowCount - 1 ? selected + 1 : 0;
		tblGrid.setRowSelectionInterval(next, next);

	}

	private void goPrevious() {
		int rowCount = tblGrid.getModel().getRowCount();
		int selected = tblGrid.getSelectedRow();

		if (selected == -1)
			return;

		int previous = selected > 0 ? selected - 1 : rowCount - 1;
		tblGrid.setRowSelectionInterval(previous, previous);
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	public ColumnList getColumnLista() {
		return columnLista;
	}

	public void setColumnLista(ColumnList columnLista) {
		this.columnLista = columnLista;
	}
//test
}

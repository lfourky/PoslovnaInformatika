package model.bean;

import annotation.Column;
import annotation.ID;
import annotation.Table;

@Table(name="poslovna_informatika.catalog_item")
public class CatalogItem {

	@ID(name = "id_catalog_item")
	@Column(name = "id_catalog_item")
	private Long catalogId;
	
	@Column(name="id_catalog")
	private Long id;

	
	
	@Column(name = "price")
	private Double price;

}

package model.bean;

import annotation.Column;
import annotation.ID;
import annotation.Table;

@Table(name="poslovna_informatika.group")
public class Group {

	@ID(name = "id_group")
	@Column(name="id_group")
	private Long id;

	@Column(name = "name")
	private String name;

}

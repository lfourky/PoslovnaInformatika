package model.bean;

import annotation.Column;
import annotation.ForeignKey;
import annotation.ID;
import annotation.Table;

@Table(name="poslovna_informatika.tax_rate")
public class TaxRate {

	@ID(name = "id_tax_rate")
	@Column(name="id_tax_rate")
	private Long id;
	
	@Column(name="id_tax")
	private Long idTax;

	@Column(name = "rate")
	private String rate;
	
	@Column(name = "expiry_date")
	private String date;

}

package util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import database.DBConnection;

public class Lookup {
	public static String getTaxName(String idTax) throws SQLException {
		String naziv = "";
		if (idTax == "")
			return naziv;
		PreparedStatement stmt = DBConnection.getConnection().prepareStatement(
				"SELECT * FROM tax WHERE id_tax = ?");
		stmt.setString(1, idTax);
		ResultSet rset = stmt.executeQuery();
		while (rset.next()) {
			naziv = rset.getString("name");
		}
		rset.close();
		stmt.close();
		return naziv;
	}
	
	public static String getGroupName(String idCompany) throws SQLException {
		String naziv = "";
		if (idCompany == "")
			return naziv;
		PreparedStatement stmt = DBConnection.getConnection().prepareStatement(
				"SELECT name FROM group WHERE id_group = ?");
		stmt.setString(1, idCompany);
		System.out.println(stmt);
		ResultSet rset = stmt.executeQuery();
		while (rset.next()) {
			naziv = rset.getString("name");
		}
		rset.close();
		stmt.close();
		return naziv;
	}
	
	public static String getCompanyName(String idCompany) throws SQLException {
		String naziv = "";
		if (idCompany == "")
			return naziv;
		PreparedStatement stmt = DBConnection.getConnection().prepareStatement(
				"SELECT * FROM company WHERE id_company = ?");
		stmt.setString(1, idCompany);
		ResultSet rset = stmt.executeQuery();
		while (rset.next()) {
			naziv = rset.getString("name");
		}
		rset.close();
		stmt.close();
		return naziv;
	}
	
	public static String getCatalogName(String idCompany) throws SQLException {
		String naziv = "";
		if (idCompany == "")
			return naziv;
		PreparedStatement stmt = DBConnection.getConnection().prepareStatement(
				"SELECT * FROM catalog WHERE id_catalog = ?");
		stmt.setString(1, idCompany);
		ResultSet rset = stmt.executeQuery();
		while (rset.next()) {
			naziv = rset.getString("acting_date");
		}
		rset.close();
		stmt.close();
		return naziv;
	}
	

}
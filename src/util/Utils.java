package util;

import java.util.LinkedList;
import java.util.List;

import model.bean.Data;

public class Utils {

	public static String[] listToArray(List<String> aList) {
		String[] anArray = new String[aList.size()];
		for (int i = 0; i < anArray.length; i++)
			anArray[i] = aList.get(i);
		return anArray;
	}

	/**
	 * For tables that have primary key
	 * @param values
	 * @return
	 */
	public static String[] linkedListToArray(Integer id, LinkedList<Data> values){
		String[] anArray = new String[values.size()+1];
		anArray[0] = id.toString();
		for(int i=0; i<values.size(); i++)
			anArray[i+1] = values.get(i).getValue();
		return anArray;
	}
	
	/**
	 * For tables that don't have primary key (they have foreign key only)
	 * @param values
	 * @param Noki
	 * @return Novica
	 */
	public static String[] linkedListToArray(LinkedList<Data> values){
		String[] anArray = new String[values.size()+1];
		for(int i=0; i<values.size(); i++)
			anArray[i] = values.get(i).getValue();
		return anArray;
	}
	
}